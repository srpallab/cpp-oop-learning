#include <iostream>

class NumberOneClass {
public:
  int NumberOne;
  NumberOneClass(int number) {
    NumberOne = number;
  }
  
  friend void swapNumbers(NumberOneClass);
};

class NumberTwoClass {
public:
  int NumberTwo;
  NumberTwoClass(int number) {
    NumberTwo = number;
  }
  friend void swapNumbers(NumberTwoClass);
};

void swapNumbers(NumberOneClass *One, NumberTwoClass *Two) {
  int temp;
  temp = One->NumberOne;
  One->NumberOne = Two->NumberTwo;
  Two->NumberTwo = temp;
}

int main(){
  NumberOneClass numOne = NumberOneClass(3);
  NumberTwoClass numTwo = NumberTwoClass(5);

  std::cout << "Before Swap Numbers" << std::endl;
  std::cout << "Number One: " << numOne.NumberOne << " and Number Two is: " << numTwo.NumberTwo << std::endl;
  swapNumbers(&numOne, &numTwo);
  std::cout << "After Swap Numbers" << std::endl;
  std::cout << "Number One: " << numOne.NumberOne << " and Number Two is: " << numTwo.NumberTwo << std::endl;
}
