#include <iostream>

using namespace std;

class Factorial {
public:
  int Number, FactorialNumber = 1;
  Factorial(int number) {
    Number = number;
    FindFactorial();
  }

  void FindFactorial() {
    for (int i = 1; i <= Number; ++i) {
      FactorialNumber = FactorialNumber * i;
    }
  }
};


int main() {
  Factorial factorial = Factorial(5);
  cout << factorial.FactorialNumber << endl;
  return 0;
}
