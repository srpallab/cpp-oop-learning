#include <iostream>

float area(float height, float base);
float area(float radius);
double area(double length, double breadth);

int main() {
  std::cout << "Triangle Area is: " << area(10.0, 6.0) << std::endl;
  std::cout << "Circle Area is: " << area(10.5) << std::endl;
  std::cout << "Rectangle Area is: " << area(10.5, 25.5) << std::endl;
  return 0;
}


float area(float height, float base) {
  return (height * base) / 2;
}

float area(float radius) {
  return 2 * 3.1416 * radius * radius;
}

double area(double length, double breadth) {
  return length * breadth;
}
