#include <iostream>

class StudentClass {
public:
  std::string Name;
  std::string Semester;
  int RollNo;
  std::string Section;
  int BanglaMark;
  int EnglishMark;
  int MathMark;
  int ScienceMark;
  int ExtraMark;
  
  StudentClass(std::string name, std::string sem, int roll, std::string sec,
	       int bm, int em, int mm, int sm, int exm) {
    Name = name;
    Semester = sem;
    RollNo = roll;
    Section = sec;
    BanglaMark = bm;
    EnglishMark = em;
    MathMark = mm;
    ScienceMark = sm;
    ExtraMark = exm;
  };

  int TotalMark() {
    return BanglaMark + EnglishMark + MathMark + ScienceMark + ExtraMark;
  }
};


int main() {
  StudentClass pal = StudentClass("Pal", "2nd", 20, "A-1", 90, 88, 100, 80, 88);
  std::cout << "Name: " << pal.Name << std::endl;
  std::cout << "Semester: " << pal.Semester << std::endl;
  std::cout << "Roll No: " << pal.RollNo << std::endl;
  std::cout << "Section: " << pal.Section << std::endl;
  std::cout << "Total Marks: " << pal.TotalMark() << std::endl;
  return 0;
}
