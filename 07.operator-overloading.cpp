#include <iostream>
#include <string>

std::string operator+(std::string &a, std::string &b) {
  return a + b;
}

int main() {
  std::string a, b, c;
  a = "Hello ";
  b = "World";
  c = a + b;
  std::cout << c << std::endl;
  return 0;
}
