#include <iostream>

class AbstractClass {
  virtual float Area() = 0;
};

class Triangle:AbstractClass {
public:
  float Height, Base;
  
  Triangle(float height, float base) {
    Height = height;
    Base = base;
  };

  float Area() {
    return (Height * Base) / 2;
  }
};


class Circle:AbstractClass {
public:
  float Radius;

  Circle(float radius) {
    Radius = radius;
  };

  float Area() {
    return 2 * 3.1416 * Radius * Radius;
  }
};


class Rectangle:AbstractClass {
public:
  float Length, Breadth;
  Rectangle(float length, float breadth) {
    Length = length;
    Breadth = breadth;
  };

  float Area(){
    return Length * Breadth;
  }
};


int main() {
  Triangle t = Triangle(10.0, 6.0);
  Circle c = Circle(25.5);
  Rectangle r = Rectangle(10.5, 25.5);

  std::cout << "Triangle Area is: " << t.Area() << std::endl;
  std::cout << "Circle Area is: " << c.Area() << std::endl;
  std::cout << "Rectangle Area is: " << r.Area() << std::endl;
  
  return 0;
}
