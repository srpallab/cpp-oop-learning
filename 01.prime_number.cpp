#include <iostream>
#include <cmath>

using namespace std;

class PrimeNumber {
public:
  int Number;

  PrimeNumber(int primeNumber) {
    Number = primeNumber;
  }

  void IsPrimeNumber() {
    int sqrtPrimeNumber;
    // Finding the square root of a number.
    // This logic is for larger number.
    sqrtPrimeNumber = sqrt(Number);
    // If any number is divider by any number before 
    // its square root number, its not a prime Number.
    for(int i = 2; i <= sqrtPrimeNumber; i++) {
      if (Number % i == 0){
	cout << "Not a Prime Number." << endl;
	return;
      }
    }
    cout << "Is a Prime Number." << endl;
  }
};

int main() {
  int inputNumber;
  // Taking input number.
  cin >> inputNumber;
  PrimeNumber prime = PrimeNumber(inputNumber);
  prime.IsPrimeNumber();
  
  return 0;
}
