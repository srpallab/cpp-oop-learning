#include <iostream>

class Complex {
public:
  int Real;
  int Img;
  Complex(){
    Real = 0;
    Img = 0;
  }
  Complex(int real, int img) {
    Real = real;
    Img = img;
  };

  void Display(){
    char ch;
    if (Img >= 0) {
      ch = '+';
    } else {
      ch = '-';
    }

    std::cout << Real << ch << abs(Img) << "i" << std::endl;
  }
  friend Complex sum(Complex, Complex);
};

Complex sum(Complex obj1, Complex obj2) {
  Complex c3;
  c3.Real = obj1.Real + obj2.Real;
  c3.Img = obj1.Img + obj2.Img;
  return c3;
}

int main() {
  Complex c1 = Complex(10, 20);
  Complex c2 = Complex(12, 24);
  Complex c4 = sum(c1, c2);
  c1.Display();
  c2.Display();
  c4.Display();
  return 0;
}
